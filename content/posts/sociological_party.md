---
title: "Sociological_party"
date: 2020-03-28T13:30:51Z
draft: false
---

How might we do that?

Well, let me tell you a wee story, and perhaps this will give you some idea of what I am trying to do here.  When Skype first started, it was the first time that we could speak to each other, at long distance for no cost. Before that telephone calls were quite expensive and international calls were very expensive. And this cost, meant that we didn't connect for long (if at all).  So when Skype started the way that we communicated, changed. And some people pushed this, and woudl spend hours chatting to each other, whislt they did other tasks about the house.

Now, might we do this with video calls?

Usually we migth experience conference calls as work. We might set a formal time. Make sure we have done our make up, and that the other person can't see the washing up or the comfy pants we are wearing.  I want us to change that.  Lets hang out, in a fluid way. Create spaces online where we can easily chat for ages whilst we share time together.

And yes, we might do that as a phoen call, taking turns to speak to one other.  How migh we do this in groups, fluid groups?

And what is a party?

A party - for me, has music. So we have a technical challenge to be able to share music that people in the same room can hear. It is not a big challenge. But will requires some thinking about, because of feedback loops.
and because of synchronising.

A party might have different spaces and different groups. For me it is about meeting new and interesting people.   I really enjoy brinign people together that I believe will get along.  And when people mix at parties, the groups change fluidly. A virtual party needs to reflect that too, somehow.

And, as a shy person, I need to be able to check out room a bti and not necessarily commit or disturb.  I like to stand at the edge of a space. Perhaps that is because I am autistic , I dont know.  Now in a virtual party I woudl lvoe to be able to do that with text chatting, and smapling the video and/or audio of the room.  This would also help with the challenge of 'noise' on the line, whcih at th emoment is endemic to conference call systems.

I also want to be able to navigate through a space, as if it is a house.  I want to enjoy different people in groups chatting with each other and make way way through and discover interesting rooms and interesting groupings of people.